<?php
/**
 * User: rulopimentel
 * Date: 19/01/15
 * Time: 02:59 PM
 */

require 'vendor/autoload.php';


error_reporting(E_ALL);
ini_set('display_errors', 1);

date_default_timezone_set("UTC");
date_default_timezone_set("America/Mexico_City");

// http://mitrabajo.sinapsistecnoartisticas.com/
//Todo Establecer constantes globales para estados de mensaje
//Todo Configurar ambientes de desarrollo y producción
//Todo Comprar certificado para SSL para nuestro sitio web
//Todo corregir el paso de estados 400, 401, 500 en todo el API y verificar el mensaje de error desde Android
//Todo cifrar token en base de datos.
//Todo Documentar código.
//Todo agregar trigger para registrar último acceso del usuario en la función de autenticación

$fpLog = fopen('logs/log-' . date('Ymd'), 'c');
fseek($fpLog, -1, SEEK_END);

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(array(
    'mode' => 'development',
    'debug' => true,
    'log.enabled' => true,
    'log.level' => \Slim\Log::DEBUG,
    'log.writer' => new \Slim\LogWriter($fpLog)
));

$app->contentType('application/json; charset=utf-8');
$app->group('/v1', function () use ($app) {


    function getConnection()
    {

        $conn = new PDO('mysql:host=localhost;dbname=fulcrono_miTrabajo', 'fulcrono_trabajo', '687_Edfdk');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->exec("SET CHARACTER SET utf8");
        return $conn;
    }

    function write_log()
    {

        $app = \Slim\Slim::getInstance();
        $request = $app->request;
        $response = $app->response;


        $ip = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?:
            getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');

        $formato = "[%d] %s \"%s\" %s :: [%s] %s\n";
        $message = sprintf($formato, time(), date('Y-m-d H:i:s T', time()), $request->getPathInfo(), $response->getStatus(), $ip, isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : null);
        $app->getLog()->debug($message);
    }

    /**
     * Función que se ejecuta despues de cada llamada para la realización del log
     */
    $app->hook('slim.after.dispatch', function () use ($app) {
        write_log();
    });

    /**
     * Autenticar mediante token.
     */
    function authenticate()
    {

        $headers = apache_request_headers();
        $app = \Slim\Slim::getInstance();
        $request = $app->request;


        if (isset($headers['Authorization'])) {
            $user = $request->headers['Php-Auth-User'];
            $token = $request->headers['Php-Auth-Pw'];
            try {
                $db = getConnection();
                $stmt = $db->prepare("SELECT * from Usuarios where user = :user AND token = :token");
                $stmt->bindParam("user", $user);
                $stmt->bindParam("token", $token);
                $result = $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt = null;
                $db = null;
                if ($result == false || count($rows) == 0) {
                    $app->response()->status(401);
                    write_log();
                    echo json_encode(array('estado' => false, 'msg' => 'No autorizado'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
                    $app->stop();
                }
            } catch (PDOException $e) {
                $app->response()->status(404);
                $app->stop();
            }
        } else {
            $app->response()->status(401);
            write_log();
            echo json_encode(array('estado' => false, 'msg' => 'No autorizado'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            $app->stop();
        }
    }

    /**
     * Autentica mediante token antes de registrar al usuario
     */
    function authenticateSMS()
    {

        $key1 = "@%rh8";
        $key2 = "_h0Rg";

        $headers = apache_request_headers();
        $app = \Slim\Slim::getInstance();
        $request = $app->request;

        if (isset($headers['Authorization'])) {
            $user = $request->headers['Php-Auth-User'];
            $token = $request->headers['Php-Auth-Pw'];
            $pass = sha1($key1 . $user . $key2);
            if ($token != $pass) {
                $app->response()->status(401);
                write_log();
                echo json_encode(array('estado' => false, 'msg' => 'No autorizado'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
                $app->stop();
            }
        } else {
            $app->response()->status(401);
            write_log();
            echo json_encode(array('estado' => false, 'msg' => 'No autorizado'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            $app->stop();
        }
    }

    /**
     * POST /registro
     * Se genera PIN de cuatro dígitos, se envía a través de Twilio Server y se alamancena el registro del usuario en
     * base de datos para su posterior comprobación
     */
    $app->post('/registro', 'authenticateSMS', function () use ($app) {
        $random_number = mt_rand(1, 9);
        for ($i = 0; $i < 3; $i++) {
            $random_number .= mt_rand(0, 9);
        }

        try {

            $user = $app->request->headers['Php-Auth-User'];
            $key1 = "[*]U#_";
            $key2 = $random_number;
            $token = sha1($key1 . $user . $key2);

            $db = getConnection();
            $stmt = $db->prepare("REPLACE INTO Usuarios(user, pin, token) values(:user, :pin, :token)");
            $stmt->bindParam("user", $user);
            $stmt->bindParam("pin", $random_number);
            $stmt->bindParam("token", $token);
            $result = $stmt->execute();
            $stmt = null;
            $db = null;

            if ($result) {
                $account_sid = 'AC5aa1754c6ec2aaa099a71cbb0e8c9f19';
                $auth_token = 'e9c1a8a42397e7dbd1471c81a1639bc4';

                $client = new Services_Twilio($account_sid, $auth_token);
                $client->account->messages->create(array(
                    'To' => $app->request->headers['Php-Auth-User'],
                    'From' => "+18329811868",
                    'Body' => "MiTrabajo by --Raul Colorado--. Código de verificación " . $random_number,
                ));

                echo json_encode(array('estado' => true, 'msg' => 'Autenticado'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            } else {
                $app->response()->status(404);
                echo json_encode(array('estado' => false, 'msg' => 'Error en consulta de BD'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
                $app->stop();
            }
        } catch (PDOException $e) {
            $app->response()->status(404);
            echo json_encode(array('estado' => false, 'msg' => $e), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            $app->stop();
        }
    });


    /**
     * GET /notificaciones
     * Obtenemos los settings de notificaciones del usuario
     */
    $app->get('/notificaciones', 'authenticate', function () use ($app) {
        $user = $app->request->headers['Php-Auth-User'];

        try {
            $db = getConnection();
            $stmt = $db->prepare("SELECT * from Notificaciones where user = :user ");
            $stmt->bindParam("user", $user);
            $result = $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt = null;
            $db = null;
            if ($result && count($rows) == 1) {

                echo json_encode(array('estado' => true, 'msg' => 'Autorizado',
                    'notificarEmpleos' => (bool)$rows[0]['notificarEmpleos'],
                    'notificarEvaluaciones' => (bool)$rows[0]['notificarEvaluaciones'],
                    'notificarMensajes' => (bool)$rows[0]['notificarMensajes'],
                    'notificarPostulantes' => (bool)$rows[0]['notificarPostulantes']), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            } else {
                echo json_encode(array('estado' => false, 'msg' => 'No se ha podido ejecutar la consulta'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            }

        } catch (PDOException $e) {
            $app->response()->status(404);
            $app->stop();
        }
    });

    /**
     * POST /notificacionesEmpleo
     * Actualizamos los settings de las notificaciones de Empleo
     */

    $app->post('/notificacionesEmpleo', 'authenticate', function () use ($app) {

        $user = $app->request->headers['Php-Auth-User'];

        $notificarEmpleos = $app->request()->params('notificarEmpleos');
        $ocupaciones = $app->request()->params('ocupaciones');
        $localidades = $app->request()->params('localidades');



        try {

            $db = getConnection();
            $db->beginTransaction();

            $stmt = $db->prepare("DELETE FROM NotificacionesHasOcupaciones WHERE user = :user");
            $stmt->bindParam("user", $user);
            $stmt->execute();

            $stmt = $db->prepare("DELETE FROM NotificacionesHasLocalidades WHERE user = :user");
            $stmt->bindParam("user", $user);
            $stmt->execute();

            foreach($ocupaciones as $valor) {
                $stmt = $db->prepare("INSERT INTO NotificacionesHasOcupaciones (user, idOcupacion) VALUES (:user, :idOcupacion)");
                $stmt->bindParam("user", $user);
                $stmt->bindParam("idOcupacion", $valor);
                $stmt->execute();
            }

            foreach($localidades as $valor) {
                $stmt = $db->prepare("INSERT INTO NotificacionesHasLocalidades (user, idLocalidad) VALUES (:user, :idLocalidad)");
                $stmt->bindParam("user", $user);
                $stmt->bindParam("idLocalidad", $valor);
                $stmt->execute();
            }


            $stmt = $db->prepare("REPLACE INTO Notificaciones (user, notificarEmpleos) values (:user, :notificarEmpleos)");
            $stmt->bindParam("user", $user);
            $stmt->bindParam("notificarEmpleos", $notificarEmpleos);
            $result = $stmt->execute();

            $db->commit();

            $stmt = null;
            $db = null;
            if ($result) {
                echo json_encode(array('estado' => true, 'msg' => 'Datos actualizados'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            }
            else {
                echo json_encode(array('estado' => false, 'msg' => 'Problema al insertar en BD'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            }
        } catch (PDOException $e) {
            $db->rollback();
            $app->response()->status(404);
            echo json_encode(array('estado' => false, 'msg' => $e->getMessage()), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            $app->stop();
        }


    });

    /**
     * POST /notificaciones
     * Actualizamos los settings de las notificaciones
     */

    $app->post('/notificaciones', 'authenticate', function () use ($app) {

        $user = $app->request->headers['Php-Auth-User'];
        $notificarEmpleos = $app->request()->params('notificarEmpleos');
        $notificarEvaluaciones = $app->request()->params('notificarEvaluaciones');
        $notificarMensajes = $app->request()->params('notificarMensajes');
        $notificarPostulantes = $app->request()->params('notificarPostulantes');

        try {
            $db = getConnection();
            $stmt = $db->prepare("REPLACE INTO Notificaciones (user, notificarEmpleos, notificarEvaluaciones, notificarMensajes, notificarPostulantes) values (:user, :notificarEmpleos, :notificarEvaluaciones, :notificarMensajes, :notificarPostulantes)");
            $stmt->bindParam("user", $user);
            $stmt->bindParam("notificarEmpleos", $notificarEmpleos);
            $stmt->bindParam("notificarEvaluaciones", $notificarEvaluaciones);
            $stmt->bindParam("notificarMensajes", $notificarMensajes);
            $stmt->bindParam("notificarPostulantes", $notificarPostulantes);
            $result = $stmt->execute();
            $stmt = null;
            $db = null;
            if ($result) {
                echo json_encode(array('estado' => true, 'msg' => 'Infromación Actualizada'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            }
            else {
                echo json_encode(array('estado' => false, 'msg' => 'Problema al insertar en BD'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            }
        } catch (PDOException $e) {
            $app->response()->status(404);
            echo json_encode(array('estado' => false, 'msg' => $e->getMessage()), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            $app->stop();
        }

    });

    /**
     * POST /usuario
     * Se realiza la comprobación de dos pasos a través del PIN que envía el usuario.
     * Se crean un token de comunicación entre el cliente y el servidor
     * Se generan registros para el usuario
     */

    $app->post('/usuario', 'authenticateSMS', function () use ($app) {

        $user = $app->request->headers['Php-Auth-User'];
        $pin = $app->request()->params('pin');

        try {
            $db = getConnection();
            $stmt = $db->prepare("SELECT * from Usuarios where user = :user AND pin = :pin");
            $stmt->bindParam("user", $user);
            $stmt->bindParam("pin", $pin);
            $result = $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt = null;
            $db = null;
            if ($result && count($rows) == 1) {
                //Creamos configuración de notificaciones inicial
                $db = getConnection();
                $stmt = $db->prepare("INSERT IGNORE INTO Notificaciones (user) VALUES(:user)");
                $stmt->bindParam("user", $user);
                $stmt->execute();
                $stmt = null;
                $db = null;
                echo json_encode(array('estado' => true, 'msg' => 'Autorizado', 'token' => (string)$rows[0]['token']), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            } else {
                echo json_encode(array('estado' => false, 'msg' => 'No autorizado', 'token' => ''), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
            }

        } catch (PDOException $e) {
            $app->response()->status(404);
            $app->stop();
        }
    });

    /**
     * Llamada a recursos no válidos
     */

    $app->notFound(function () use ($app) {
        $app->response()->status(404);
        write_log();
        echo json_encode(array('estado' => false, 'msg' => 'Petición no encontrada'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
        $app->stop();
    });

    /**
     * Recurso principal del API
     */

    $app->get('/', function () use ($app) {
        echo json_encode(array('estado' => true, 'API' => 'miTrabajo', 'Versión' => '0.1', 'Create by' => 'Raúl Pimentel - rulo.pimentel@gmail.com'), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
    });

    $app->run();
});